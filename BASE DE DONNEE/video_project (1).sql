-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 27 Janvier 2019 à 22:47
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `video_project`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:simple_array)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `birthday`, `roles`, `password`) VALUES
(1, 'Alex', 'Rigueur', 'alex.rigueur@gmail.com', '2014-03-12 14:33:00', 'ROLE_USER', '$2y$13$veA9rY/1qrRgtYPdL24HwObZOUguJDBIIqvkrxxhWvAk4siOwJozW'),
(2, 'Boris', 'Richar', 'richardu03@gmail.com', '2014-03-10 02:28:05', 'ROLE_USER', '$2y$13$veA9rY/1qrRgtYPdL24HwObZOUguJDBIIqvkrxxhWvAk4siOwJozW'),
(3, 'ilyes', 'test', 'aa@gmail.com', '2014-01-01 00:00:00', 'ROLE_USER', '$2y$13$veA9rY/1qrRgtYPdL24HwObZOUguJDBIIqvkrxxhWvAk4siOwJozW'),
(4, 'admin', 'admin', 'admin@admin.com', '2014-01-01 00:00:00', 'ROLE_ADMIN', '$2y$13$8jcvBmu13p92671lmVy93OIEheAWZf4evYWe6f65kL8FEHKPYl58y'),
(6, 'Alex', 'Rigueur', 'test@test.com', '2019-01-01 00:00:00', 'ROLE_USER', '$2y$13$Sxc/uQO/cMv44uC4xNnMs.nvxxAaNkBzocp0CHb/po2XUIdyExuiu'),
(7, 'Alain', 'Patrice', 'alain@gmail.com', '1990-01-01 00:00:00', 'ROLE_USER', '$2y$13$4S2irSh0Okg1vvBpjxbUL.lCwXnNPB2jWnyURX7f3z4txmRvq/dzS');

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
`id` int(6) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` tinytext NOT NULL,
  `link` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `resume` longtext NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `video`
--

INSERT INTO `video` (`id`, `categorie`, `user_id`, `title`, `link`, `created_at`, `resume`) VALUES
(1, 'Rap', 1, 'Kaaris - AieAieOuille', 'https://www.youtube.com/watch?v=LcbZQ-njDaM', '2019-01-05 09:00:00', 'test1'),
(2, 'Tuto', 2, 'TUTO PHP - Créer un espace membre 1/3 (Inscription)', 'https://www.youtube.com/watch?v=s7qtAnH5YkY', '2019-01-09 00:00:00', 'test2'),
(4, 'Blague', 3, 'NOUVELLE ÉMISSION ! - BLAGUES PERDUES 1', 'https://www.youtube.com/watch?v=iMfNZcfb_9w', '2019-01-01 00:00:00', 'test3'),
(6, 'Rap', 2, 'Booba Petite Fille', 'https://www.youtube.com/watch?v=QHuo2pIyTH8', '2019-01-23 17:16:00', 'Extrait de l''album "Trône" de Booba, disponible ici : https://Booba.lnk.to/TroneYD \r\nAbonne-toi : http://bit.ly/2aIsz16 \r\nBooba - Petite Fille (Clip officiel) \r\nRéalisateur : Hossegor (Guillaume Cagniard)'),
(14, 'Film', 3, 'Bande Annonce Spider Man VF 2019', 'https://www.youtube.com/watch?v=FguIk-SEkWI', '2019-01-01 00:00:00', 'Le premier trailer de Spider-Man Far from Home, le 3 juillet 2019 au cinéma !'),
(15, 'News', 7, 'Razer Blade Stealth (2019) - The Almost Perfect Ultrabook!', 'https://www.youtube.com/watch?v=ddPnVvRz3F8', '2019-01-01 00:00:00', 'Review of the 2019 Razer Blade Stealth. It''s a remarkable Ultrabook with  great performance for a 13" thin and light laptop. It packs one of the most powerful MX150 gpus while being super portable. It comes at a hefty price tag so let''s find out if i');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `video`
--
ALTER TABLE `video`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_23A0E66A76ED395` (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `video`
--
ALTER TABLE `video`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
