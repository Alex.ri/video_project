<?php

namespace App\Controller;
use App\Form\EditVideoType;
use App\Form\RegisterUserType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\LoginUserType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ProfileUserType;
use App\Repository\UserRepository;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Session\Session;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="security")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegisterUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('notice', 'Success registration');
            return $this->redirectToRoute('home');
        }


        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $user = new User();
        $form = $this->createForm(LoginUserType::class, $user);
        $form->handleRequest($request);

        return $this->render('security/login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'form' => $form->createView(),
        ]);
    }






    /**
     * @Route("/admin", name="admin")
     */
    public function admin(Request $request, UserRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();

        // Get all entities from Video table
        $users = $em->getRepository(User:: class)->findAll();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // $this->redirectToRoute(‘register_sucess’);
        }

        return $this->render('security/admin.html.twig', array(
            'form' => $form->createView(),
            'controller_name' => 'VideoController',
            'user' => $users ,
        ));
    }

    /**
     * @Route("/admin/all_categorie", name="allcategorie")
     */
    public function allcategorie(Request $request, UserRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();

        // Get all entities from Video table
        $users = $em->getRepository(User:: class)->findAll();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // $this->redirectToRoute(‘register_sucess’);
        }

        return $this->render('security/allcategorieadmin.html.twig', array(
            'form' => $form->createView(),
            'controller_name' => 'VideoController',
            'user' => $users ,
        ));
    }

    /**
     * @Route("/admin/all_video", name="allvideo")
     */
    public function allvideo(Request $request, UserRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();

        // Get all entities from Video table
        $users = $em->getRepository(User:: class)->findAll();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // $this->redirectToRoute(‘register_sucess’);
        }

        return $this->render('security/all_video_admin.html.twig', array(
            'form' => $form->createView(),
            'controller_name' => 'VideoController',
            'user' => $users ,
        ));
    }




}
