<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Video;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\Request;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Annotations\Annotation;
use App\Form\EditVideoType;
use App\Entity\User;
use App\Form\ProfileUserType;
use App\Repository\UserRepository;
use Psr\Log\LoggerInterface;
class VideoController extends AbstractController
{
    /**
     * @Route("/video", name="video")
     */
    public function index(LoggerInterface $logger, Request $request, VideoRepository $videoRepository)
    {
        // Get the Doctrine Manager
        $em = $this->getDoctrine()->getManager();

        // Get all entities from Video table
        $videos = $em->getRepository(Video:: class)->findAll();

        // Send to the View template 'video/index.html.twig' an array of content
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($video);
            $entityManager->flush();
            $logger->info('new video added');
            $this->addFlash('notice', 'Video has been added');
        // $this->redirectToRoute(‘register_sucess’);
        }
        return $this->render('video/index.html.twig', array(
            'form' => $form->createView(),
            'controller_name' => 'VideoController',
            'videos' => $videos ,
        ));
    }

    /**
     * @Route("/video/remove/{id}", name="video_remove")
     * @ParamConverter("video", options={"mapping"={"id"="id"}})
     */
    public function remove(LoggerInterface $logger,Video $video, EntityManagerInterface $entityManager )
    {
        $entityManager ->remove($video);
        $entityManager ->flush();
        $logger->info('video deleted');
        $this->addFlash('notice', 'Video has been deleted!');
        return $this->redirectToRoute( 'home');
    }

    /**
     * @Route("/user/remove/{id}", name="user_remove")
     * @ParamConverter("user", options={"mapping"={"id"="id"}})
     */
    public function removeuser(User $user, EntityManagerInterface $entityManager )
    {
        $entityManager ->remove($user);
        $entityManager ->flush();
        $this->addFlash('notice', 'User has been deleted');
        return $this->redirectToRoute( 'home');
    }

    /**
     * @Route("/categorie/remove/{id}", name="categorie_remove")
     * @ParamConverter("user", options={"mapping"={"id"="id"}})
     */
    public function removecategorie(Video $video, EntityManagerInterface $entityManager )
    {
        $entityManager ->remove($video);
        $entityManager ->flush();
        $this->addFlash('notice', 'Categorie has been deleted');
        return $this->redirectToRoute( 'home');
    }



    /**
     * @Route("/video/edit/{id}", name="edit")
     */
    public function edit(Video $video, Request $request, EntityManagerInterface $entityManager, $id)
    {


        // Send to the View template 'video/index.html.twig' an array of content
        $video = $this->getVideos();
        $form = $this->createForm(EditVideoType::class, $video);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($video);
            $entityManager->flush();
            // $this->redirectToRoute(‘register_sucess’);
        }
        return $this->render('video/edit.html.twig', [
            'form' => $form->createView(),
    ]);
    }

    /**
     * @Route("/myvideo", name="myvideo")
     */
    public function myvideo(Request $request, EntityManagerInterface $entityManager)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('security/myvideo.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/video/list/{id}", name="list")
     */
    public function list(Request $request, UserRepository $userRepository, int $id)
    {
        $em = $this->getDoctrine()->getManager();

        // Get all entities from Video table

        $users = $em->getRepository(User:: class)->findById($id);

        if (!$em) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        return $this->render( 'video/sort.html.twig',[
            'users' => $users]);
    }

    /**
     * @Route("/video/categorie/{categorie}", name="categorie")
     */
    public function categorie(Request $request, UserRepository $userRepository, $categorie)
    {
        $manager = $this->getDoctrine()->getManager();
        $users = $manager->getRepository(User::class)->findAllVideoByCategorie($categorie);

        return $this->render( 'video/categorie.html.twig',[
            'users' => $users]);

    }


}
